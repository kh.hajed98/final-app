package com.example.afinal;


import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class MainActivity extends AppCompatActivity {
     private EditText email,pass;
     private TextView sig;
     private Button login;
    private FirebaseAuth mAuth;

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef = database.getReference("user Data");

    private   NotificationManagerCompat notificat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sig = (TextView) findViewById(R.id.sign);
        email = (EditText) findViewById(R.id.mail) ;
        pass = (EditText) findViewById(R.id.password) ;
        login = (Button) findViewById(R.id.button) ;
        mAuth = FirebaseAuth.getInstance();











    login.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            log();

        }
    });

        sig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                signup();

            }
        });

    }

    private void nnotify(String value) {
        Intent homealert = new Intent(this, carinfo.class);
        PendingIntent content = PendingIntent.getActivity(this,0,homealert,0);
        Bitmap img = BitmapFactory.decodeResource(getResources(),R.drawable.car);
        Notification notification = new NotificationCompat.Builder(this,app.CHANNEL_1_ID).setSmallIcon(R.drawable.car)
                .setContentTitle("Alert!!!!!!")
                .setContentText(value)
                .setLargeIcon(img)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setColor(Color.BLUE)
                .setContentIntent(content)
                .build();
        notificat.notify(1,notification);
    }


    private void log() {
        String mm = email.getText().toString().trim();
        String ll = pass.getText().toString().trim();
        if (email.getText().toString().isEmpty()  || pass.getText().toString().isEmpty()){

            Toast.makeText(this, "please enter your email & password", Toast.LENGTH_SHORT).show();
        }else{
            mAuth.signInWithEmailAndPassword(mm,ll).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), "Loging In...", Toast.LENGTH_LONG).show();
                        home();

                        //  notify
                        notificat = NotificationManagerCompat.from(getApplicationContext());




                        myRef.child("alert").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                // This method is called once with the initial value and again
                                // whenever data at this location is updated.
                                String value = dataSnapshot.getValue(String.class);
                                if(value.toString().equals("An unknowing person in your car")){
                                    nnotify(value);
                                    Toast.makeText(getApplicationContext(), "An unknowing person in your car",Toast.LENGTH_LONG).show();
                                } if (value.toString().equals("Someone open your car door")){
                                    nnotify(value);
                                    Toast.makeText(getApplicationContext(), "Someone open your car door",Toast.LENGTH_LONG).show();
                                } if (value.toString().equals("your car make an accident!")){
                                    nnotify(value);
                                    Toast.makeText(getApplicationContext(), "your car make an accident!",Toast.LENGTH_LONG).show();
                                }

                            }

                            @Override
                            public void onCancelled(DatabaseError error) {
                                // Failed to read value
                                Toast.makeText(getApplicationContext(), "error conection !",Toast.LENGTH_SHORT).show();


                            }
                        });



                    }else{
                        Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
                    }
                }
            });}
    }

    private void home() {
        Intent A =new Intent(this, home.class);
        startActivity(A);
    }


    private void signup() {
        Intent T = new Intent(this, signup.class);
        startActivity(T);
    }


}
