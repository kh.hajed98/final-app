package com.example.afinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;


public class home extends AppCompatActivity implements View.OnClickListener{

    private Switch opendoor, camera, sms;
    private Button save;

      FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("user Data");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        save = (Button) findViewById(R.id.save);
        opendoor = (Switch) findViewById(R.id.door);
        camera =  (Switch) findViewById(R.id.cam);
        sms =  (Switch) findViewById(R.id.sms);
        camera.setOnClickListener(this);
        opendoor.setOnClickListener(this);
        save.setOnClickListener(this);
        sms.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.sms){
            if(sms.isChecked()) {
                Toast.makeText(getApplicationContext(), "sms notifier is ON ", Toast.LENGTH_LONG).show();
            }else {
                Toast.makeText(getApplicationContext(), "sms notifier is OFF ", Toast.LENGTH_LONG).show();
            }

        }
        if(v.getId()== R.id.save){

            open_carinfo();
            Toast.makeText(getApplicationContext(),"saving configuration... ",Toast.LENGTH_LONG).show();
        }
        if(v.getId() == R.id.cam){
            if(camera.isChecked()){
                myRef.child("cam").child("cam sensor").setValue("activated");
               Toast.makeText(getApplicationContext(),"system activated for cam  sensor ",Toast.LENGTH_LONG).show();
            }else{
                myRef.child("cam").child("cam sensor").setValue("deactivated");
                Toast.makeText(getApplicationContext(),"system deactivated for cam  sensor ",Toast.LENGTH_LONG).show();
            }
        }

        if(v.getId() == R.id.door){
            if(opendoor.isChecked()){
                myRef.child("door").child("door sensor").setValue("activated");

                Toast.makeText(getApplicationContext(),"system activated for open door sensor ",Toast.LENGTH_LONG).show();
            }else{
                myRef.child("door").child("door sensor").setValue("deactivated");
                Toast.makeText(getApplicationContext(),"system deactivated for open door sensor ",Toast.LENGTH_LONG).show();

            }
        }

    }

    private void open_carinfo() {
        Intent intent = new Intent(this, carinfo.class);
        startActivity(intent);
    }
}
