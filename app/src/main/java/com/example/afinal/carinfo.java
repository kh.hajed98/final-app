package com.example.afinal;

import android.app.Notification;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class carinfo extends AppCompatActivity {
private Button track,buscan,camera;
private ImageView setting;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("user Data");




    private NotificationManagerCompat notificat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carinfo);
        track =(Button) findViewById(R.id.track);
        buscan = (Button) findViewById(R.id.buscan);
        camera = (Button) findViewById(R.id.cam);
        setting = (ImageView) findViewById(R.id.setting);
        notificat = (NotificationManagerCompat) NotificationManagerCompat.from(this);
        track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                track_car();
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                config();
            }
        });
buscan.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        stopcar();
    }
});
camera.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        cameraa();
    }
});




      /*  myRef.child("alert").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                if(value.toString().equals("tr")){
                nnotify();}else{
                    Toast.makeText(getApplicationContext(), "nooooooooooo",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });*/




    }

    private void cameraa() {
        Intent intent = new Intent(getApplicationContext(), photo.class);
        startActivity(intent);
    }

    private void config() {
        Intent intent = new Intent(this,home.class);
        startActivity(intent);
    }

    private void stopcar() {
        if(buscan.getText().toString().equals("Stop my car")) {
            myRef.child("stopcar").setValue("car off");
            buscan.setText("turn on my car");
        }else{
            myRef.child("stopcar").setValue("car on");
            buscan.setText("Stop my car");
        }

    }

   /* private void nnotify() {
        Notification notification = new NotificationCompat.Builder(this,app.CHANNEL_1_ID).setSmallIcon(R.drawable.ic_arrow)
                .setContentTitle("hi")
                .setContentText("hhhhhhhhhhhhhhh")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .build();
        notificat.notify(1,notification);
    }*/


    private void track_car() {
        Intent inten = new Intent(getApplicationContext(), Mapslocation.class);
        startActivity(inten);

        Toast.makeText(getApplicationContext(), "tracking your car",Toast.LENGTH_SHORT).show();
    }
}
